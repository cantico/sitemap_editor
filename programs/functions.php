<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';


/**
 * Translate string with gettext
 * 
 * @return string
 */ 
function smed_translate($str, $str_plurals = null, $number = null) {
	
	if ($translate = bab_functionality::get('Translate/Gettext'))
	{
		/* @var $translate Func_Translate_Gettext */
		$translate->setAddonName('sitemap_editor');
		
		return $translate->translate($str, $str_plurals, $number);
	}
	
	return $str;
}


function smed_requireCredential() {
    bab_requireCredential(smed_translate('You must be logged in to access this page.'));
    
}


/**
 * @return bab_addonInfos
 */ 
function smed_getAddon() {
	return bab_getAddonInfosInstance('sitemap_editor');
}

/**
 * @param	string	$url
 */ 
function smed_goto($url) {
	header('location:'.$url);
	exit;
}

/**
 * @return smed_Controller
 */
function smed_controller()
{
    require_once dirname(__FILE__).'/controller.class.php';
    return bab_getInstance('smed_Controller');
}



function smed_addPageInfo($message)
{
	$_SESSION['smed_msginfo'][] = $message;
}


/**
 * Redirects to the specified url or action.
 * This function makes the current script die.
 * 
 * @param Widget_Action|string $url
 */
function smed_redirect($url, $message = null)
{
	global $babBody;
	
	if (null === $url) {
		die('<script type="text/javascript">history.back();</script>');
	}

	if ($url instanceof Widget_Action) {
		$url = $url->url();
	}
	if (!empty($babBody->msgerror)) {
		$url .=  '&msgerror=' . urlencode($babBody->msgerror);
	}
	if (isset($message)) {
		$lines = explode("\n", $message);
		foreach ($lines as $line) {
			smed_addPageInfo($line);
		}
	}
	
	header('Location: ' . $url);
	die;
}





/**
 * @return Func_Widgets
 */
function smed_Widgets()
{
	return bab_functionality::get('Widgets');
}





/**
 * Get article category by name in a category
 * @param	string $name
 * @return int
 */
function smed_getCategoryByName($id_category, $name)
{
	require_once $GLOBALS['babInstallPath'].'utilit/artapi.php';
	
	$name = strtolower($name);
	$categories = bab_getChildrenArticleCategoriesInformation($id_category, false, false);
	foreach($categories as $id_category => $category)
	{
		if (bab_removeDiacritics(strtolower($category['title'])) === bab_removeDiacritics($name))
		{
			return $id_category; 
		}
	}
	
	return null;
}





/**
 * @param string $url
 * @return bool
 */
function smed_urlIsExternal($url)
{
	global $babUrl;
	if (($urlComponents = parse_url($url)) !== false) {
		if (isset($urlComponents['scheme'])) {
			if (substr($url, 0, strlen($babUrl)) !== $babUrl) {
				return true;
			}
		}
	}

	return false;
}





/**
 * @param string $url
 * @return string
 */
function smed_absolutizeUrl($url)
{
	if (($urlComponents = parse_url($url)) !== false) {
		if (!isset($urlComponents['scheme'])) {
			$path = isset($urlComponents['path']) ? $urlComponents['path'] : '';
			$query = isset($urlComponents['query']) ? '?' . $urlComponents['query'] : '';
			$url = $GLOBALS['babUrl'] . $path . $query;
		}
	}

	return $url;
}





/**
 * @param smed_Node $node
 * @return string
 */
function smed_nodeType(smed_Node $node)
{
	return $node->getContentType();
} 


/**
 * Get id topic of a node
 * @param smed_Node $node
 * @return string
 */
function smed_getTopicId(smed_Node $node)
{
    $target = $node->getTargetId();
    
    $id_topic = substr($target, strlen('babArticleTopic_'));
    
    if (false === $id_topic) {
        throw new Exception(sprintf('Failed to get topic ID from target node ID %s', $target));
    }
    
    return $id_topic;
}



/**
 * get numeric id from uid string
 * @return int
 */
function smed_getIdFromUid($uid)
{
	$arr = explode('_', $uid);
	$id = (int) end($arr);
	
	if (0 === $id)
	{
		return null;
	}
	
	return $id;
}


function smed_sitemapGetRessource() {
	global $babDB;
	$res = $babDB->db_query('
			SELECT
			id,
			name,
			description
			FROM
			smed_sitemap
			ORDER BY name
			');

	return $res;
}


/**
 * Get node from functionality from content_type in database
 * @param string $content_type
 * 
 * @return Func_SitemapEditorNode
 */
function smed_getNodeFunctionality($content_type)
{
    if (empty($content_type)) {
        throw new Exception(smed_translate('Content type must not be empty'));
    }
    
    $default = bab_functionality::get('SitemapEditorNode');
    /*@var $default Func_SitemapEditorNode */
    $types = $default->getAllContentTypes();
    
    if (!isset($types[$content_type])) {
        throw new Exception(sprintf(smed_translate('This content type is not suported: %s'), $content_type));
    }
    
    return $types[$content_type]['functionality'];
}









/**
 * @param string $language
 * @return string
 */
function smed_getCountryFromLanguage($language)
{
    if ('en' === $language) {
        return 'GB';
    }

    return $language;
}