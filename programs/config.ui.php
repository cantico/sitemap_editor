<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/base.ui.php';



class smed_ConfigEditor extends smed_Editor
{

	public function __construct($id = null, Widget_Layout $layout = null)
	{
		$W = smed_Widgets();

		parent::__construct($id, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
		
		$this->colon();



		$this->setName('config');
		$this->setHiddenValue('tg', bab_rp('tg'));


		$this->addItem($this->rewriting());
		$this->addItem($this->appendId());
		$this->addItem($this->createCategoryNode());
		$this->addItem($this->moveAdminTopic());

		$this->setSaveAction(smed_controller()->Config()->save());
		$this->setCancelAction(smed_controller()->Config()->cancel());
	}




	protected function rewriting()
	{
		$W = $this->widgets;
		return $this->labelledField(
			smed_translate('Enable rewriting'),
			$W->CheckBox(),
			'rewriting',
			smed_translate('The rewriting need mod_rewrite apache module installed and a compatible skin with <base href="{ babUrl }" /> into page.html')
		);
	}
	
	
	
	protected function appendId()
	{
		$W = $this->widgets;
		return $this->labelledField(
			smed_translate('Append node ID to the urls'),
			$W->CheckBox(),
			'appendId'
		);
	}
	
	
	protected function createCategoryNode()
	{
		$W = $this->widgets;
		return $this->labelledField(
			smed_translate('Allow creation of topic category node into sitemap'),
			$W->CheckBox(),
			'createCategoryNode'
		);
	}
	
	
	protected function moveAdminTopic()
	{
		$W = $this->widgets;
		return $this->labelledField(
			smed_translate('Allow topics modifications in admin articles management to match the sitemap tree'),
			$W->CheckBox(),
			'moveAdminTopic'
		);
	}
	
}



