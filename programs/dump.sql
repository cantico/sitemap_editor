CREATE TABLE `smed_sitemap` (
  `id` 						int(10) 		unsigned 	NOT NULL 	auto_increment,
  `name` 					varchar(255) 				NOT NULL 	default '',
  `description` 			text 						NOT NULL,
  PRIMARY KEY  (`id`) 
);



CREATE TABLE `smed_nodes` (
  `id_function`				varchar(64) 				NOT NULL 	default ''			COMMENT 'Link to Ovidentia core sitemap node or new node name',
  `id_parent`				varchar(64) 				NOT NULL 	default ''			COMMENT 'Link to parent in core sitemap or in node list',
  `id_sitemap`	 			int(10) 		unsigned 	NOT NULL 	default '0'			COMMENT 'link to smed_sitemap.id',
  `node_type`	 			tinyint(1) 		unsigned 	NOT NULL 	default '0'			COMMENT 'Node type : smed_Node::ISCORE = 1, smed_Node::ISMODIFIED = 2, smed_Node::ISNEW = 3',
  `sort_type` 				char(8)	 				 				default NULL		COMMENT 'childnodes sort method : smed_Node::SORT_UNORDERED, smed_Node::SORT_ALPHA, smed_Node::SORT_FIXED_TOP, smed_Node::SORT_FIXED_BOTTOM',
  `disabled` 				tinyint(1) 		unsigned 	NOT NULL	default '0'			COMMENT '0 | 1',
  `disabled_rewrite` 		tinyint(1) 		unsigned 	NOT NULL	default '0'			COMMENT '0 | 1',
  `url` 					text		 				NOT NULL 						COMMENT	'mandatory if node_type = smed_Node::ISNEW',
  `classnames` 				varchar(255) 				NOT NULL 	default ''			COMMENT 'icon classname | spaces separated css classes',
  `popup`		 			tinyint(1) 		unsigned 	NOT NULL 	default '0'			COMMENT '1: open in a new window, 0: same window',
  `menuignore`	 			tinyint(1) 		unsigned 	NOT NULL 	default '0'			COMMENT '1: ignore node is sitemapMenu, 0: display node in sitemapMenu',
  `breadcrumbignore`	 	tinyint(1) 		unsigned 	NOT NULL 	default '0'			COMMENT '1: ignore node is sitemapPosition, 0: display node in sitemapPosition',
  `rewriteignore`           tinyint(1)      unsigned    NOT NULL    default '0'         COMMENT '1: ignore node is rewrite path, 0: display node in rewrite path',
  `page_title`				varchar(255)				NOT NULL	default ''			COMMENT 'Title used for referencing',
  `page_keywords`			text						NOT NULL						COMMENT 'Keywords used for referencing',
  `page_description`		text						NOT NULL						COMMENT 'description used for referencing',
  `rewrite_name`			varchar(255)				NOT NULL	default ''			COMMENT 'Name used for url rewriting',
  `target`					varchar(64)					NOT NULL	default ''			COMMENT 'Target function id',
  `targetchildnodes`		tinyint(1) 		unsigned 	NOT NULL 	default '0',
  `content_type`			varchar(64)					NOT NULL	default ''			COMMENT 'content type of node : container|topic|category|url|ref|....',
  `setlanguage`             varchar(5)                  NOT NULL    default ''          COMMENT 'language code. Will set site language if the node is in the used path',
  `langid`                  varchar(64)                 NOT NULL    default ''          COMMENT 'Node id unique per language code',
  `contenttypes`            text                        NOT NULL                        COMMENT 'coma separated list of allowed content types for subnodes creation',
  PRIMARY KEY  (`id_sitemap`, `id_function`),
  KEY `id_parent` (`id_parent`)
);



CREATE TABLE `smed_node_labels` (
  `id` 						int(10) 		unsigned 	NOT NULL 	auto_increment,
  `id_sitemap`	 			int(10) 		unsigned 	NOT NULL 	default '0'			COMMENT 'link to smed_sitemap.id',
  `id_function` 			varchar(64) 				NOT NULL 	default ''			COMMENT 'Link to Ovidentia core sitemap node or new node name',
  `lang`		 			varchar(32) 				NOT NULL 	default '',
  `name` 					varchar(255) 				NOT NULL 	default '',
  `description` 			text 						NOT NULL,
  PRIMARY KEY  (`id`), 
  UNIQUE KEY `ident` (`id_sitemap`, `id_function`, `lang`)
);


CREATE TABLE `smed_sort` (
  `id_sitemap`	 			int(10) 		unsigned 	NOT NULL 	default '0'			COMMENT 'link to smed_sitemap.id',
  `id_parent`				varchar(64) 				NOT NULL 	default ''			COMMENT 'Link to parent in smed_nodes.id_function',
  `id_function`				varchar(64) 				NOT NULL 	default ''			COMMENT 'Link to smed_nodes.id_function',
  `sort_index`	 			int(10) 		unsigned 	NOT NULL 	default '0'			COMMENT 'Sort index',
  KEY `id_parent` (`id_parent`),
  UNIQUE KEY `node` (`id_sitemap`, `id_function`),
  KEY `sort_index` (`sort_index`)
);


CREATE TABLE `smed_cache` (
  `id_sitemap`	 			int(10) 		unsigned 	NOT NULL 	default '0'			COMMENT 'link to smed_sitemap.id',
  `data`					longtext					NOT NULL, 				
  `uid_functions`			int(10) 		unsigned	NOT NULL 	default '0',
  `creation_date` 			datetime 				 	NOT NULL 	default '0000-00-00 00:00:00',
  UNIQUE KEY `uid_functions` (`id_sitemap`, `uid_functions`),
  KEY `creation_date` (`creation_date`)
);
