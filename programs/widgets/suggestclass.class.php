<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

bab_Widgets()->includePhpClass('Widget_SuggestLineEdit');





/**
 * A smed_SuggestClass
 */
class smed_SuggestClass extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{
    /**
     * @param string $id
     */
	public function __construct($id = null)
	{
		parent::__construct($id);
		$this->setMultiple(' ');
		$this->setMinChars(0);
	}

	/**
	 * (non-PHPdoc)
	 * @see Widget_SuggestLineEdit::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'smed-suggestclass';
		return $classes;
	}





	/**
	 * Send suggestions
	 */
	public function suggest()
	{
		if (false !== $keyword = $this->getSearchKeyword()) {

			bab_functionality::includefile('Icons');
			$iconReflector = new ReflectionClass('Func_Icons');
			$constants = array_flip($iconReflector->getConstants());

			bab_Sort::natcasesort($constants);

			$i = 0;
			$emails = array();
			foreach ($constants as $key => $c) {

			    if ($keyword !== '' && strpos($key, $keyword) === false) {
			        continue;
			    }
				$i++;
				if ($i > Widget_SuggestLineEdit::MAX) {
					break;
				}

				parent::addSuggestion(
					$key,
					str_replace('_', '-', strtolower($c)),
					$c,
					'',
				    $key
				);
			}

			parent::sendSuggestions();
		}
	}


	/**
	 * @param Widget_Canvas $canvas
	 */
	public function display(Widget_Canvas $canvas)
	{
		$this->suggest();
		return parent::display($canvas);
	}
}