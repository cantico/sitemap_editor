<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/sitemap.class.php';

function smed_nodePickerSubTree($currentNode, $nodes, $nodeId, $reset = false) {
    /* Save the node summit not to go it through several times */
    static $nodesSummit = null;
    if ($nodesSummit === null) {
        $nodesSummit = array();
    }
    if ($reset) {
        $nodesSummit = array();
    }
    $nodesSummit[$currentNode] = 1;
    $node = $nodes[$currentNode];

    if ($node->getId() == $nodeId)
    {
        return '';
    }

    $str = '';
    $str .= '<li>';
    if ($node->getName())
    {
        $str .= '<a href="#" title="' . $currentNode . '">' . $node->getName() . '</a>';
    }

    /* List of sub-nodes */
    $ul = '';
    foreach ($nodes as $nodeTmp) {
        if ($nodeTmp->getParentId() == $currentNode && !isset($nodesSummit[$nodeTmp->getId()])) {
            $ul.= smed_nodePickerSubTree($nodeTmp->getId(), $nodes, $nodeId);
        }
    }

    if ($ul != '') {
        $str .= '<ul>' . $ul . '</ul>';
    }

    $str .= '</li>';

    return $str;
}

function smed_nodePickerTree()
{
    $nodeTmp = smed_Sitemap::getModifiedIterator();
    $nodes = array();
    foreach($nodeTmp as $node)
    {
        $nodes[$node->getId()] = $node;
    }

    $nodeId = bab_rp('basenode', null);

    return '<ul>'.smed_nodePickerSubTree('Custom', $nodes, $nodeId, true).'</ul>';
}

switch (bab_rp('idx')) {
    case 'node':
        echo smed_nodePickerTree();
        die();
}
