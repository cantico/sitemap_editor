<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



class Func_ContextActions_SitemapNode extends Func_ContextActions
{

    public function getDescription()
    {
        return bab_translate('Match a sitemap node');
    }

    /**
     * Get a pattern or string to match a CSS class
     * @return string
     */
    public function getClassSelector()
    {
        return '[class*=smed-sitemapnode-]';
    }

    protected function getIdFromClasses(Array $classes)
    {
        foreach ($classes as $className) {
            $m = null;
            if (preg_match('/smed-sitemapnode-(.+)/', $className, $m)) {
                return $m[1];
            }
        }

        return null;
    }

    /**
     * Get the list of actions
     * @param array $classes all css classes found on the element
     * @param bab_url $url Page url where the actions will be added
     * @return Widget_Action[]
     */
    public function getActions(Array $classes, bab_url $url)
    {
        require_once dirname(__FILE__).'/node.class.php';
        
        $W = bab_Widgets();
        $nodeId = $this->getIdFromClasses($classes);
        $actions = array();
        
        $node = smed_Node::get(1, $nodeId);

        if (!isset($node)) {
            return array();
        }    
        
        if ($node->canRead()) {
            $actions[] = $W->Action()
            ->setMethod('addon/sitemap_editor/main', 'node.display', array('node' => $nodeId))
            ->setTitle(sprintf(smed_translate('Menu item "%s"'), $node->getName()))
            ->setIcon(Func_Icons::ACTIONS_DOCUMENT_PROPERTIES);
        }
        
        if ($node->canUpdate()) {
            $actions[] = $W->Action()
            ->setMethod('addon/sitemap_editor/main', 'node.edit', array('node' => $nodeId))
            ->setTitle(sprintf(smed_translate('Edit menu item "%s"'), $node->getName()))
            ->setIcon(Func_Icons::ACTIONS_DOCUMENT_EDIT);
        }
        
        return $actions;
    }
}