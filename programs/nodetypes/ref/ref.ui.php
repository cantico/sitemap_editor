<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */








class smed_RefNodeEditor extends smed_NodeEditor
{
    public function __construct($id = null, Widget_Layout $layout = null, smed_Node $node = null)
    {
        parent::__construct($id, $layout, $node);
        $this->setHiddenValue('node[type]', 'ref');

        $this->generalSection->addItem($this->sort_type(),0);
        $this->generalSection->addItem($this->Description(),0);
        $this->generalSection->addItem($this->Label(), 0);
        $this->generalSection->addItem($this->widthchildnodes(), 0);
        $this->generalSection->addItem($this->sitemapitem(), 0);

    }


    protected function sort_type()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Sort childnodes'),
            $W->Select()
            ->addOption(smed_Node::SORT_ALPHA, smed_translate('By node label'))
            ->addOption(smed_Node::SORT_UNORDERED, smed_translate('Same as target')),
            __FUNCTION__
        );
    }


    public function sitemapitem()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Sitemap item'),
            $W->SitemapItemPicker()->setSitemap('core'),
            'target'
        );
    }


    public function widthchildnodes()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('With childnodes'),
            $W->Checkbox(),
            'targetchildnodes'
        );
    }
}
