<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */






class smed_UrlNodeEditor extends smed_NodeEditor
{

    public function __construct($id = null, Widget_Layout $layout = null, smed_Node $node = null)
    {
        parent::__construct($id, $layout, $node);

        $W = $this->widgets;

        $this->generalSection->addItem($this->Description(),0);
        $this->generalSection->addItem($this->Label(), 0);
        $this->generalSection->addItem($this->Url(), 0);
        $this->generalSection->addItem($this->ApplicationUrl());
        $this->generalSection->addItem($this->ApplicationId());
        $this->generalSection->addItem($this->disabledRewrite());


        $this->setHiddenValue('node[type]', 'url');
        $this->setValue('node/url', 'http://');
    }

    protected function disabledRewrite()
    {
        $W = $this->widgets;
        return
        $this->labelledField(
            smed_translate('Disable rewriting'),
            $W->CheckBox(),
            'disabled_rewrite'
        );
    }


    protected function ApplicationId()
    {
        $W = $this->widgets;
        return $W->Hidden()->setName('application');
    }


    protected function ApplicationUrl()
    {
        $W = $this->widgets;


        $checkbox = $this->labelledField(
            smed_translate('Add this url to the application addon for access control on link.'),
            $chkwidget = $W->CheckBox(),
            'application_url'
        );

        $aclwidget = $W->Acl()->setTitle(smed_translate('Access rights on link'))->setName('application_rights');
        $acl = $W->Frame()->addItem($aclwidget);

        $vbox = $W->VBoxItems($checkbox, $acl)->setVerticalSpacing(1, 'em');

        $chkwidget->setAssociatedDisplayable($acl, array('1'));

        return $vbox;
    }

}
