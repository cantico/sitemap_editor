<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

bab_functionality::includeOriginal('SitemapEditorNode');


class Func_SitemapEditorNode_Topic extends Func_SitemapEditorNode
{
    public function getDescription()
    {
        return smed_translate('Article topic');
    }
    
    
    public function getClassName()
    {
        return Func_Icons::OBJECTS_PUBLICATION_TOPIC;
    }
    
    
    protected function includeNodeEditor()
    {
        parent::includeNodeEditor();
        require_once dirname(__FILE__).'/topic.ui.php';
    }
    
    
    /**
     * Get interface to display in node edit mode
     * @param smed_Node $node
     * @return smed_TopicNodeEditor
     */
    public function getEditor($content_type, smed_Node $node = null)
    {
        
        
        $this->includeNodeEditor();
        
        if (isset($node)) {
            
            $editor = new smed_EditTopicNodeEditor(null, null, $node);
            
            try {
                $topic = smed_getTopicId($node);
            } catch (Exception $e) {
                bab_debug($e->getMessage());
                $topic = 0;
            }
            
            $editor->setValue('node/topic', $topic);
            
            
            require_once $GLOBALS['babInstallPath'].'utilit/artapi.php';
            require_once $GLOBALS['babInstallPath'].'admin/acl.php';
            
            try {
                $options = bab_getTopicArray($topic);
            } catch (Exception $e) {
                bab_debug($e->getMessage());
                $options = array();
            }
            $editor->setValues($options, array('node', 'topic_options'));
            $editor->setValue('node/topic_options/access_rights/topicsview', aclGetRightsString(BAB_TOPICSVIEW_GROUPS_TBL, $topic));
            $editor->setValue('node/topic_options/access_rights/topicssub', aclGetRightsString(BAB_TOPICSSUB_GROUPS_TBL, $topic));
            $editor->setValue('node/topic_options/access_rights/topicsmod', aclGetRightsString(BAB_TOPICSMOD_GROUPS_TBL, $topic));
            $editor->setValue('node/topic_options/access_rights/topicsman', aclGetRightsString(BAB_TOPICSMAN_GROUPS_TBL, $topic));
            
            return $editor;
            
        }
        
    
        $editor = new smed_NewTopicNodeEditor();
        
        $editor->setValue('node/topic_options/access_rights/topicsview', BAB_ALLUSERS_GROUP);
        $editor->setValue('node/topic_options/access_rights/topicssub', BAB_ADMINISTRATOR_GROUP);
        $editor->setValue('node/topic_options/access_rights/topicsmod', BAB_ADMINISTRATOR_GROUP);
        $editor->setValue('node/topic_options/access_rights/topicsman', BAB_ADMINISTRATOR_GROUP);
        
        return $editor;

    }
    
    
    
    /**
     * Before sitemap node is saved
     * The $node['id'] will exists also if the node is not created
     * the node will not be saved if there is a bab_saveException or a bab_accessException
     * 
     * @param array &$node         Form posted node
     * @param bool  $nodeExists    True if the node allready exists in sitemap (node modification)
     */
    public function preSave(Array &$node, $nodeExists)
    {
        $parentNode = smed_Node::getFromId(1, $node['parent']);
        $top = new smed_ArticleTopic($parentNode);
        
        if (empty($node['topic'])) {
            if (!$nodeExists && isset($node['label']) && !empty($node['label'])) {
                $node['topic'] = $top->create($node['label'], $node['description'], $node['topic_options']);
            }
        }
        $node['target'] = 'babArticleTopic_' . $node['topic'];
        
        $topic_options = $node['topic_options'];
        
        $top->update($node['topic'], $node['label'], $node['description'], $topic_options);
        
    }
    

    
    
    
    
    /**
     * Update treeview element before insert into the treeview (set custom icon ... )
     * @param smed_Node             $node
     * @param bab_TreeViewElement   $element
     */
    public function updateTreeViewElement(smed_Node $node, bab_TreeViewElement $element)
    {
        $addonInfos = bab_getAddonInfosInstance('sitemap_editor');
        $nodeImagePath = $addonInfos->getImagesPath();
    
        $element->setIcon($nodeImagePath . '/nodes/publication-topic.png');
    }
}