<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





class smed_TopicNodeEditor extends smed_NodeEditor
{

    protected $topicSection;

    public function __construct($id = null, Widget_Layout $layout = null, smed_Node $node = null)
    {
        parent::__construct($id, $layout, $node);
        $this->setHiddenValue('node[type]', 'topic');

        $W = $this->widgets;

        $this->topicSection = $W->Section(
            smed_translate('Topic options'),
            $W->VBoxItems($tabs = $W->Tabs())
        );
        $this->topicSection->setFoldable(true, true)->setName('topic_options');
        $this->addItem($this->topicSection, 1);

        $options = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

        $options->addItem($this->idsaart());
        $options->additem($this->allow_attachments());
        $options->addItem($this->allow_addImg());
        $options->addItem($this->allow_meta());

        $tabs->addItem($W->Tab(null, smed_translate('Parameters'), $options)->noTitle());
        $tabs->addItem($this->access_rights());

        $tabs->setCanvasOptions($tabs->Options()->width(550));
    }


    protected function idsaart()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
        $W = $this->widgets;

        $s = $W->Select()->addOption('', '');
        $arr = bab_WFGetApprobationsList();

        foreach($arr as $x)
        {
            $s->addOption($x['id'], $x['name']);
        }

        return $this->labelledField(
            smed_translate('Approval scheme'),
            $s,
            __FUNCTION__
        );
    }


    protected function allow_attachments()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Allow files attachments to articles'),
            $W->Checkbox()->setCheckedValue('Y')->setUncheckedValue('N'),
            __FUNCTION__
        );
    }

    protected function allow_addImg()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Allow image attachment to articles'),
            $W->Checkbox()->setCheckedValue('Y')->setUncheckedValue('N'),
            __FUNCTION__
        );
    }


    protected function allow_meta()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Allow HTML metadata to articles for a better SEO'),
            $W->Checkbox(),
            __FUNCTION__
        );
    }

    protected function Topic()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Topic'),
            $W->TopicPicker(),
            'topic'
        );
    }


    protected function access_rights()
    {
        $W = $this->widgets;
        $l = $W->VBoxLayout()->setSpacing(1,'em')->addClass('smed-topic-acl');

        $options = $l->Options();
        $options->width(100,'em');

        $l->addItem($W->Acl()->setTitle(smed_translate('Who can read articles from this topic?'))->setName('topicsview'));
        $l->addItem($W->Acl()->setTitle(smed_translate('Who can submit new articles?'))->setName('topicssub'));
        $l->addItem($W->Acl()->setTitle(smed_translate('Who can post comment?'))->setName('topicscom'));
        $l->addItem($W->Acl()->setTitle(smed_translate('Who can modify articles?'))->setName('topicsmod'));
        $l->addItem($W->Acl()->setTitle(smed_translate('Who can manage this topic?'))->setName('topicsman'));

        return $W->Tab(null, smed_translate('Access rights'), $l)->setName(__FUNCTION__)->noTitle();
    }


}










class smed_NewTopicNodeEditor extends smed_TopicNodeEditor
{
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        parent::__construct($id, $layout);

        $W = $this->widgets;

        $tabs = $W->Tabs()
        ->addItem($W->Tab()->noTitle()->setLabel(smed_translate('Create'))
            ->additem($this->Label())
            ->addItem($this->Description())
        )
        ->addItem($W->Tab()->noTitle()->setLabel(smed_translate('Link to existing topic'))->addItem($this->Topic()));

        $tabs->setCanvasOptions($tabs->Options()->width(550));

        $this->generalSection->addItem($tabs, 0)->addClass('icon-left-16 icon-16x16 icon-left');
    }
}






class smed_EditTopicNodeEditor extends smed_TopicNodeEditor
{
    public function __construct($id = null, Widget_Layout $layout = null, smed_Node $node = null)
    {
        parent::__construct($id, $layout, $node);

        $this->generalSection->addItem($this->Description(),0);
        $this->generalSection->addItem($this->Label(), 0);
        $this->generalSection->addItem($this->Topic(), 0)->addClass('icon-left-16 icon-16x16 icon-left');
    }
}
