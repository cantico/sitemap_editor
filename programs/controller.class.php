<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';




class smed_Controller extends bab_Controller 
{

    
    /**
     * (non-PHPdoc)
     * @see bab_Controller::getControllerTg()
     */
    protected function getControllerTg()
    {
        return 'addon/sitemap_editor/main';
    }
    
    
    /**
     * (non-PHPdoc)
     * @see bab_Controller::getObjectName()
     */
    protected function getObjectName($classname)
    {
        $prefix = strlen('smed_Ctrl');
        return strtolower(substr($classname, $prefix));
    }
    

	/**
	 * @return smed_CtrlNode
	 */
	public function Node($proxy = true)
	{
		require_once dirname(__FILE__) . '/node.ctrl.php';
		return bab_Controller::ControllerProxy('smed_CtrlNode', $proxy);
	}




	/**
	 * @return smed_CtrlConfig
	 */
	public function Config($proxy = true)
	{
		require_once dirname(__FILE__) . '/config.ctrl.php';
		return bab_Controller::ControllerProxy('smed_CtrlConfig', $proxy);
	}


}

